import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateStudentComponent } from './components/create-student/create-student.component';
import { StudentDetailsComponent } from './components/student-details/student-details.component';
import { StudentListComponent } from './components/student-list/student-list.component';

import { HttpClientModule } from '@angular/common/http';
import { UpdateStudentComponent } from './components/update-student/update-student.component';
import { CreateCourseComponent } from './components/create-course/create-course.component';
import { UpdateCourseComponent } from './components/update-course/update-course.component';
import { CourseListComponent } from './components/course-list/course-list.component';


@NgModule({
  declarations: [
    AppComponent,
    CreateStudentComponent,
    StudentDetailsComponent,
    StudentListComponent,
    UpdateStudentComponent,
    CreateCourseComponent,
    UpdateCourseComponent,
    CourseListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
