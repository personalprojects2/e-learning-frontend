import {Course} from './course';

export class Student {
  id: number;
  name: string;
  email: string;
  userName: string;
  password: string;
  dateOfBirth: string;
  gender: string;
  courses:[Course];

}
