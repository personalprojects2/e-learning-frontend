export class Course {
  id : number;
  name : string ;
  description : string;
  publishDate : string ;
  last_updated : string;
  totalHours : string;
  instructor : string;

}
