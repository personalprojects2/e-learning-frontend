import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CreateStudentComponent} from './components/create-student/create-student.component';
import {StudentListComponent} from './components/student-list/student-list.component';
import {StudentDetailsComponent} from './components/student-details/student-details.component';
import {UpdateStudentComponent} from './components/update-student/update-student.component';
import {CourseListComponent} from './components/course-list/course-list.component';
import {CreateCourseComponent} from './components/create-course/create-course.component';
import {UpdateCourseComponent} from './components/update-course/update-course.component';


const routes: Routes = [
  { path: '', redirectTo: 'students', pathMatch: 'full' },
  { path: 'students', component: StudentListComponent },
  { path: 'student/add', component: CreateStudentComponent },
  { path: 'student/update/:id', component: UpdateStudentComponent },
  { path: 'student/details/:id', component: StudentDetailsComponent },

  { path: 'courses', component: CourseListComponent },
  { path: 'course/add', component: CreateCourseComponent },
  { path: 'course/update/:id', component: UpdateCourseComponent },

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
