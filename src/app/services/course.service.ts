import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  private baseUrl = 'https://e-learning-321.herokuapp.com/course/list';

  private getCourseUrl = 'https://e-learning-321.herokuapp.com/course/get';

  private updateCourseUrl = 'https://e-learning-321.herokuapp.com/course/update';

  private deleteCourseUrl = 'https://e-learning-321.herokuapp.com/course/delete';

  private getSaveUrl = 'https://e-learning-321.herokuapp.com/course/save';

  constructor(private http: HttpClient) { }

  getCourse(id: number): Observable<any> {
    return this.http.get(`${this.getCourseUrl}/${id}`);
  }

  createCourse(course: Object): Observable<Object> {
    return this.http.put(`${this.getSaveUrl}`, course);
  }

  updateCourse(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.updateCourseUrl}/${id}`, value);
  }

  deleteCourse(id: number): Observable<any> {
    return this.http.delete(`${this.deleteCourseUrl}/${id}`, { responseType: 'text' });
  }

  getCoursesList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
