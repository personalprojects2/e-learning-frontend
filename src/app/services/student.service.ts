import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  private baseUrl = 'https://e-learning-321.herokuapp.com/student/list';

  private getStudentUrl = 'https://e-learning-321.herokuapp.com/student/get';

  private createBaseUrl = 'https://e-learning-321.herokuapp.com/student/save';

  private getDeleteUrl = 'https://e-learning-321.herokuapp.com/student/delete';

  private getUpdateUrl = 'https://e-learning-321.herokuapp.com/student/update';

  private registerCourseUrl = 'https://e-learning-321.herokuapp.com/student/register';

  private unRegisterCourseUrl = 'https://e-learning-321.herokuapp.com/student/unregister';

  private getRegisteredCoursesUrl = 'https://e-learning-321.herokuapp.com/student/courses';

  constructor(private http: HttpClient) { }

  getStudent(id: number): Observable<any> {
    return this.http.get(`${this.getStudentUrl}/${id}`);
  }

  createStudent(student: Object): Observable<Object> {
    return this.http.put(`${this.createBaseUrl}`, student);
  }

  updateStudent(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.getUpdateUrl}/${id}`, value);
  }

  deleteStudent(id: number): Observable<any> {
    return this.http.delete(`${this.getDeleteUrl}/${id}`, { responseType: 'text' });
  }

  getStudentsList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }


  registerCourse(studentId: number,courseId: number ): Observable<Object> {
    return this.http.put(`${this.registerCourseUrl}?student_id=${studentId}&course_id=${courseId}`,courseId);
  }


   unRegisterCourse(studentId: number,courseId: number ): Observable<Object> {
    return this.http.put(`${this.unRegisterCourseUrl}?student_id=${studentId}&course_id=${courseId}`,courseId);
  }

  getRegisteredCourses(id: number): Observable<any> {
    return this.http.get(`${this.getRegisteredCoursesUrl}/${id}`);
  }
}
