import { Component, OnInit } from '@angular/core';
import {StudentService} from '../../services/student.service';
import {Router} from '@angular/router';
import {Student} from '../../models/student';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

  students: Observable<Student[]>;

  constructor(private studentService: StudentService,
              private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.students = this.studentService.getStudentsList();
    console.log('=========>',this.students);
  }

  deleteStudent(id: number) {
    this.studentService.deleteStudent(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  studentDetails(id: number){
    this.router.navigate(['student/details', id]);
  }

  updateStudent(id: number){
    this.router.navigate(['student/update', id]);
  }
}
