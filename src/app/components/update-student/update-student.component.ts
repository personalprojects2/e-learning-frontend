import { Component, OnInit } from '@angular/core';
import {Student} from '../../models/student';
import {ActivatedRoute, Router} from '@angular/router';
import {StudentService} from '../../services/student.service';
import {CourseService} from '../../services/course.service';
import {Observable} from 'rxjs';
import {Course} from '../../models/course';

@Component({
  selector: 'app-update-student',
  templateUrl: './update-student.component.html',
  styleUrls: ['./update-student.component.css']
})
export class UpdateStudentComponent implements OnInit {

  id: number;
  student: Student;
  studentCourses : Course;
  courses: Observable<Course[]>;
  studentCoursesIds:any[]= [] ;

  constructor(private route: ActivatedRoute,private router: Router,
              private studentService: StudentService,private courseService: CourseService,) { }

  ngOnInit() {
    this.student = new Student();
    this.courses = this.courseService.getCoursesList();
    this.id = this.route.snapshot.params['id'];

    this.studentService.getStudent(this.id)
      .subscribe(data => {
        console.log(data)
        this.student = data;
      }, error => console.log(error));

    this.studentService.getRegisteredCourses(this.id)
      .subscribe(data => {
        console.log(data)
        this.studentCourses = data;

        var index = 0;
        for (var studentCourse in this.studentCourses){
          this.studentCoursesIds.push(this.studentCourses[index]['id']);
          index++;
        }
      }, error => console.log(error));

  }

  updateStudent() {
    this.studentService.updateStudent(this.id, this.student)
      .subscribe(data => {console.log(data);this.gotoList();}, error => console.log(error));
    this.student = new Student();
  }

  onSubmit() {
    this.gotoList();
  }

  gotoList() {
    this.router.navigate(['/students']);
  }

  registerCourse(courseId:number)
  {
    this.studentService.registerCourse(this.id,courseId)
      .subscribe(data => {console.log(data);this.ngOnInit();}, error => console.log(error));
  }

  checkRegistered(id:number){

    if(this.studentCoursesIds.indexOf(id)>-1){
      return true;
    }
    return false;

  }

}
