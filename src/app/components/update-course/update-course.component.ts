import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CourseService} from '../../services/course.service';
import {Course} from '../../models/course';

@Component({
  selector: 'app-update-course',
  templateUrl: './update-course.component.html',
  styleUrls: ['./update-course.component.css']
})
export class UpdateCourseComponent implements OnInit {


  id: number;
  course: Course;

  constructor(private route: ActivatedRoute,private router: Router,
              private courseService: CourseService) { }

  ngOnInit() {
    this.course = new Course();

    this.id = this.route.snapshot.params['id'];

    this.courseService.getCourse(this.id)
      .subscribe(data => {
        console.log(data)
        this.course = data;
      }, error => console.log(error));
  }

  updateCourse() {
    this.courseService.updateCourse(this.id, this.course)
      .subscribe(data => {console.log(data); this.gotoList();}, error => console.log(error));
    this.course = new Course();

  }

  onSubmit() {
    this.updateCourse();
  }

  gotoList() {
    this.router.navigate(['/courses']);
  }
}
