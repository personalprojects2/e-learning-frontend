import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {StudentService} from '../../services/student.service';
import {Student} from '../../models/student';
import {Course} from '../../models/course';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css']
})
export class StudentDetailsComponent implements OnInit {

  id: number;
  student: Student;
  studentCourses : Course;
  constructor(private route: ActivatedRoute,private router: Router,
              private studentService: StudentService) { }

  ngOnInit() {
    this.student = new Student();

    this.id = this.route.snapshot.params['id'];

    this.studentService.getStudent(this.id)
      .subscribe(data => {
        console.log(data)
        this.student = data;
      }, error => console.log(error));

    this.studentService.getRegisteredCourses(this.id)
      .subscribe(data => {
        console.log(data)
        this.studentCourses = data;
      }, error => console.log(error));
  }

  list(){
    this.router.navigate(['students']);
  }

  unRegisterCourse(courseId:number)
  {
    this.studentService.unRegisterCourse(this.id,courseId)
      .subscribe(data => {console.log(data);this.ngOnInit();}, error => console.log(error));
  }
}
